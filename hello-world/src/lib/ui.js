var Chartist = require('chartist');
var $ = require('jquery')


var container = $("<div></div>");
container.attr("id", "message-container");
container.prependTo($("body"));

function message(title, text, color, icon) {
    var card = $("<div style='vertical-align: top; margin: 16px; margin-bottom: 0; background-color: " + color + ";' class='card wide'><div>")
    var inner = $("<div class='card-content white-text'></div>");
    card.append(inner);
    inner.append("<span class='card-title' style='display: flex; '><i class='material-icons' style='font-size: 32px; margin-right: 12px;'>" +
        icon + "</i>" + title + "</span>");
    inner.append("<p>" + text + "</p>");
    $("body").append(card);
}


module.exports.info = function (title, text) {
    message(title, text, "#03A9F4", "info");
}

module.exports.warning = function (title, text) {
    message(title, text, "#FFC107", "warning");
}

module.exports.error = function (title, text) {
    message(title, text, "#F44336", "error");
}



function warning(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message warning");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}

function error(text) {
    var info = $("<div></div>").appendTo("#message-container");
    info.addClass("message error");
    var t = $("<p></p>").appendTo(info);
    t.text(text);
}

donut_count = 0;
plot_count = 0;

function createCard(title, text, innerId) {
    var card = $("<div style='margin: 16px; margin-bottom: 0;' class='card primary-bg wide'><div>")
    var inner = $("<div class='card-content white-text'></div>");
    card.append(inner);
    inner.append("<span class='card-title'>" + title + "</span>");
    inner.append("<p>" + text + "</p>");
    inner.append("<div id='" + innerId + "' class='donut'></div>");
    return card;
}

module.exports.linspace = function (start, stop, n) {
    var arr = []
    var increment = (stop - start) / n;

    for (var i = 0; i < n; i++) {
        arr.push(start + i * increment);
    }

    return arr;
};

module.exports.plot = function (data, title) {
    var id = "plot_" + plot_count;
    plot_count++;

    var card = createCard(title, "", id);
    $("body").append(card);

    $("#" + id).addClass("ct-chart ct-golden-section plot");

    new Chartist.Line("#" + id, data, {
        axisX: {
            labelInterpolationFnc: function (value, idx) {
                return null;
            }
        },
        low: 0,
        high: 5
    });
}

module.exports.donut = function (text, value, pct, icon) {
    var id = "donut_" + donut_count;
    donut_count++;

    var card = createCard(text, "", id);
    $("body").append(card);

    var x1 = 220 * pct;
    var x2 = 220 * (1 - pct);

    var chart = new Chartist.Pie("#" + id, {
        series: [x1, x2],
        labels: ['', '']
    }, {
            donut: true,
            donutWidth: 20,
            startAngle: 210,
            total: 260,
            showLabel: false,
            plugins: [
                Chartist.plugins.fillDonut({
                    items: [{
                        content: '<i class="material-icons">' + icon + '</i>',
                        position: 'bottom',
                        offsetY: 10,
                        offsetX: -2
                    }, {
                        content: value
                    }]
                })
            ],
        });
}