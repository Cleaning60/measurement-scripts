oboe = require('oboe');
const EventEmitter = require('events');

defaultConfig = {
    port: "/dev/ttyACM0",
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: "none"
};

if (typeof serialConfig === 'undefined') {
    console.log("warning, serial config is not defined, using default");
    console.log(JSON.stringify(defaultConfig, null, 2));
    serialConfig = defaultConfig;
}

parser = null;
buffer = "";

serialDataEmitter = new EventEmitter();

BaseSerial = {
    read: function () {
        return new Promise((resolve) =>
            serialDataEmitter.on("data", function (data) {
                resolve(data);
            })
        );
    },
    readLine: async function () {
        return new Promise((resolve) =>
            serialDataEmitter.on("data", function (data) {
                buffer += data;
                var idx = buffer.indexOf('\n');
                if (idx >= 0) {
                    resolve(buffer.substring(0, idx));
                    if (buffer.length > idx) {
                        buffer = buffer.substring(idx + 1, buffer.length);
                    } else {
                        buffer = "";
                    }
                }
            })
        );
    },
    readJson: async function () {
        parser = oboe();

        return new Promise((resolve, reject) => {
            parser.on("done", function (json) {
                parser.abort();
                parser = null;
                resolve(json);
            });

            parser.on("fail", function (error) {
                //parser = null;
                //console.log(JSON.stringify(error, null, 2));
                //reject(error);
            })
        });
    },
    readStreamingJson: async function (pattern, callback) {
        parser = oboe();

        if (pattern != null && callback != null) {
            console.log("listening for node " + pattern);
            parser.node(pattern, function (data) {
                callback(data);
            })
        }


        return new Promise((resolve, reject) => {
            parser.on("done", function (json) {
                parser.abort();
                parser = null;
                resolve(json);
            });

            parser.on("fail", function (error) {
                parser = null;
                reject(error);
            })
        });
    }

}

AndroidSerial = {
    init: function () {

    },
    read: BaseSerial.read,
    readLine: BaseSerial.readLine,
    readJson: BaseSerial.readJson,
    readStreamingJson: BaseSerial.readStreamingJson,
    write: function (data) {
        sensor.serialWrite(data);
    }
};

DesktopSerial = {
    init: function () {
        SerialPort = require('serialport');

        port = new SerialPort(serialConfig.port, {
            baudRate: serialConfig.baudRate,
            dataBits: serialConfig.dataBits,
            stopBits: serialConfig.stopBits,
            parity: serialConfig.parity
        }, function (err) {
            if (err != null) {
                console.log("error init");
                console.log('Error: ', err);
                port = null;
                return;
            }
        });

        port.pipe(new SerialPort.parsers.Readline());

        try {
            port.on("data", function (data) {
                console.log("received data");
                if (parser != null) {
                    parser.emit("data", data + "");
                } else {
                    serialDataEmitter.emit("data", data);
                }
            });
        } catch (e) {
            console.log("unable to read from serial, " + hwSerial);
        }

    },
    read: BaseSerial.read,
    readLine: BaseSerial.readLine,
    readJson: BaseSerial.readJson,
    readStreamingJson: BaseSerial.readStreamingJson,
    write: function (data) {
        port.write(data);
    }
};



if (typeof _ANDROID === 'undefined') {
    Serial = DesktopSerial;
} else {
    Serial = AndroidSerial;
}

Serial.onDataAvailable = function () { // this is only called by Android
    if (parser != null) {
        parser.emit("data", sensor.serialRead());
    } else {
        serialDataEmitter.emit("data", sensor.serialRead());
    }
}

Serial.init();

module.exports.Serial = Serial;