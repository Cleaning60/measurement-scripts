serialConfig = {
    port: "/dev/ttyACM0",
    baudRate: 115200,
    dataBits: 8,
    stopBits: 1,
    parity: "none"
}


serial = require("./lib/serial.js").Serial;
app = require("./lib/app.js").App;
utils = require("./lib/utils.js");

(async function () {

    // use await keyword with sleep
    await utils.sleep(2000);
    serial.write("a");

    var expected = 512;
    var count = 0;

    // this time get notified for every value
    res = await serial.readStreamingJson("data[*]", function (item) {
        console.log("item " + count + " / " + expected + "  = " + item);
        app.progress(count / expected * 100);
        count++;
    });

    console.log("res " + JSON.stringify(res, 0, 2));

    app.result(res);

})();