var $ = require("jquery");
sprintf = require("sprintf-js").sprintf;
app = require("./lib/app.js").App;
ui = require("./lib/ui.js");
math = require("./lib/math.js");

require("./css/style.css");
require("materialize-css/dist/css/materialize.min.css");

require("chartist/dist/chartist.min.css");
require("chartist-plugin-fill-donut");
require("materialize-css");


if (typeof result === "undefined") {
    // the result is loaded from the fs, allowing to debug
    // actual data from the device

    try {
        result = require("../data/result.json");
    } catch (err) {
        result = {
            data: [1, 3, 2, 1, 3, 2, 2, 2, 5]
        };
    }
}


var len = result.data.length;

var voltage = [];
for (var i = 0; i < len; i++) {
    var v = result.data[i] / 1024.0 * 5; // convert to [V] unit
    voltage.push(v);
}

// plot first 50 measured voltage levels
var data = {
    series: [voltage]
};
ui.plot(data, "ADC Voltage in [V]");

avg = 0;
// calculate average
for (var i = 0; i < len; i++) {
    avg += voltage[i] / len;
}

// calculate rms
var rms = 0;
for (var i = 0; i < len; i++) {
    var tmp = voltage[i] - avg;
    var sqr = tmp * tmp;
    rms += 1 / len * sqr;
}
ui.donut("RMS AC in pct", sprintf("%.1f", (rms / 0.95) * 100), rms / 0.95, "");


rms = Math.sqrt(rms);

ui.donut("Average in [V]", sprintf("%.3f", avg), avg / 5, "");


app.csvExport("avg", avg);
app.csvExport("rms AC", rms);

ui.info("raw rms value", rms);
ui.info("average calculated", "average has been calculated exported");
ui.info("rms calculated", "rms has been calculated exported");
ui.warning("rms out of range", sprintf("rms %.3f seems to be out of range", rms));
ui.error("avg looks weird", sprintf("avg %.3f is clearly funky", avg));



app.save();