const path = require('path');
const webpack = require('webpack');
const fs = require('fs-cli');

module.exports.pack = function (done) {
    webpack({
            entry: path.resolve(__dirname, "../src/processor.js"),
            output: {
                path: target.build,
                filename: target.filename
            },
            externals: externals,
            module: {
                rules: [{
                        test: /\.css$/,
                        use: [
                            "style-loader",
                            "css-loader"
                        ],
                    },
                    {
                        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                        use: [
                            "url-loader?limit=10000&mimetype=application/font-woff"
                        ]
                    },
                    {
                        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                        use: [
                            'file-loader'
                        ]
                    }
                ]
            }
        },
        (err, stats) => {
            if (err || stats.hasErrors()) {
                console.log(stats);
            } else {
                fs.mv(path.join(target.build, target.filename), path.join(target.dist, target.filename));
                console.log("success, processor script: " + path.join(target.dist, target.filename));
                done();
            }
            // Done processing
        }
    )
};


var opts = ["android", "browser"]
if (process.argv.length != 3 || !opts.includes(process.argv[2])) {
    console.log("usage: node pack-processor (processor|android) defaulting to android");
    type = "android";
}

var type = process.argv[2];

var filename = "";
if (type == "browser") {
    externals = {
        "serialport": "SerialPort",
        "fs": "fs",
    }

    filename = "processor-browser-debug.js";
    console.log("URL for browser: file://" + path.join(__dirname, "../dist", "processor.html"));
    
} else {
    externals = {
        "serialport": "SerialPort",
        "fs": "fs",
        "./data/result-prototype.json": "result",
        "./data/result.json": "result"
    }

    filename = "processor.js";
}

var target = {
    filename: filename,
    dist: path.resolve(__dirname, "../dist"),
    build: path.resolve(__dirname, "../build")
};



module.exports.pack(function () {
    console.log("done");
});