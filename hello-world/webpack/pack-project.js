const fs = require('fs-cli');
const fs2 = require('fs');
const path = require('path');
const archiver = require('archiver');


const processor = require("./pack-processor.js");
const sensor = require("./pack-sensor.js");

processor.pack(function () {
    console.log("processor packed");

    sensor.pack(function () {
        console.log("sensor packed");
        
        var baseDir = path.join(__dirname, "../");
        var zip = path.basename(baseDir) + ".zip";
        var file = path.join(baseDir, "dist", zip);

        var archive = archiver('zip', {
            zlib: {
                level: 9
            } // Sets the compression level.
        });

        fs.rm(file);

        var s = path.resolve(__dirname, "../dist/sensor.js");
        var p = path.resolve(__dirname, "../dist/processor.js");
        var m = path.resolve(__dirname, "../src/manifest.json");
        fs.cp(m, path.join(baseDir, "dist", "manifest.json"));

        var output = fs2.createWriteStream(file);
        output.on('close', function () {
            console.log(archive.pointer() + ' total bytes');
            console.log('archiver has been finalized and the output file descriptor has closed.');
        });

        output.on('end', function () {
            console.log('Data has been drained');
        });


        archive.on('warning', function (err) {
            if (err.code === 'ENOENT') {
                console.log("ENOENT");
            } else {
                throw err;
            }
        });

        archive.on('error', function (err) {
            throw err;
        });

        archive.pipe(output);

        archive.append(fs2.createReadStream(s), {
            name: 'sensor.js'
        });


        archive.append(fs2.createReadStream(p), {
            name: 'processor.js'
        });


        archive.append(fs2.createReadStream(m), {
            name: 'manifest.json'
        });


        archive.finalize();
    });
});